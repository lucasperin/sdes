#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int s2i(char *str) {
	return (int) strtol(str, NULL, 2);
}

char* i2s(int bin, int len) {
	char * str = malloc(len*sizeof(char));
	int i;
	for(i = 0; i < len; i++) {
		int temp = (bin >> i) & 1;
		if(temp == 1){
			str[len-1-i] = '1';
		} else { 
			str[len-1-i] = '0';
		}
	}
	return str;
}

int chkparams(int argc, char **argv){
	if(argc > 3)
		return 0;
	if(strlen(argv[1]) > 10)
		return 0;
	if(strlen(argv[2]) > 8)
		return 0;
	return 1;
}

int s0[4][4] =
{
	{1, 0, 3, 2},
	{3, 2, 1, 0},
	{0, 2, 1, 3},
	{3, 1, 3, 2}
};

int s1[4][4] =
{
	{0, 1, 2, 3},
	{2, 0, 1, 3},
	{3, 0, 1, 0},
	{2, 1, 0, 3}
};

int box(int index, int sbox[4][4]) {
	printf("inde in Sbox: %s\n", i2s(index,4));
	int i = ((index >> 2 ) & 0x2) | (index & 1);
	int j = ((index >> 1 ) & 0x2) | ((index >> 1)&1);
	printf("sbox values i=%d j=%d\n", i, j);
	return sbox[i][j];
}

int ip(int data) {
	int ret = 0;
	ret = ret | ((data >> 1) & 1);
	ret = ret | (((data >> 3) & 1)<<1);
	ret = ret | (((data >> 0) & 1)<<2);
	ret = ret | (((data >> 4) & 1)<<3);
	ret = ret | (((data >> 7) & 1)<<4);
	ret = ret | (((data >> 5) & 1)<<5);
	ret = ret | (((data >> 2) & 1)<<6);
	ret = ret | (((data >> 6) & 1)<<7);
	return ret;
}

int ep(int data) {
	int ret = 0;
	ret = ret | ((data >> 3) & 1);
	ret = ret | (((data >> 0) & 1) <<1 );
	ret = ret | (((data >> 1) & 1) <<2 );
	ret = ret | (((data >> 2) & 1) <<3 );
	ret = ret | (((data >> 1) & 1) <<4 );
	ret = ret | (((data >> 2) & 1) <<5 );
	ret = ret | (((data >> 3) & 1) <<6 );
	ret = ret | (((data >> 0) & 1) <<7 );
	return ret;
}

int p4(int data) {
	int ret = 0;
	ret = ret | ((data>>3)&1);
	ret = ret | (((data>>1)&1) << 1);
	ret = ret | (((data>>0)&1) << 2);
	ret = ret | (((data>>2)&1) << 3);
	return ret & 0xF;
}

int sw(int data) {
	int ret = (data >> 4) & 0xF;
	return ret | ((data << 4) & 0xF0);
}

int ipi(int data) {
	int ret = 0;
	ret = ret | ((data >> 2) & 1);
	ret = ret | (((data >> 0) & 1)<<1);
	ret = ret | (((data >> 6) & 1)<<2);
	ret = ret | (((data >> 1) & 1)<<3);
	ret = ret | (((data >> 3) & 1)<<4);
	ret = ret | (((data >> 5) & 1)<<5);
	ret = ret | (((data >> 7) & 1)<<6);
	ret = ret | (((data >> 4) & 1)<<7);
	return ret & 0xFF;
}

int p10(int key) {
	int ret = 0;
	ret = ret | ((key >> 4) & 1);
	ret = ret | (((key >> 2) & 1)<<1);
	ret = ret | (((key >> 1) & 1)<<2);
	ret = ret | (((key >> 9) & 1)<<3);
	ret = ret | ((key & 1)<<4);
	ret = ret | (((key >> 6) & 1)<<5);
	ret = ret | (((key >> 3) & 1)<<6);
	ret = ret | (((key >> 8) & 1)<<7);
	ret = ret | (((key >> 5) & 1)<<8);
	ret = ret | (((key >> 7) & 1)<<9);
	return ret & 0x3FF;
}

int p8(int key) {
	int ret = 0;
	ret = ret | ((key >> 1) & 1);
	ret = ret | (((key >> 0) & 1)<<1);
	ret = ret | (((key >> 5) & 1)<<2);
	ret = ret | (((key >> 2) & 1)<<3);
	ret = ret | (((key >> 6) & 1)<<4);
	ret = ret | (((key >> 3) & 1)<<5);
	ret = ret | (((key >> 7) & 1)<<6);
	ret = ret | (((key >> 4) & 1)<<7);
	return ret & 0xFF;
}
int ls1(int key) {
	int ret = key << 1;
	ret = ret | ((key >>4)&1);
	return ret & 0x1F;
}

int ls2(int key) {
	int ret = key << 2;
	ret = ret | (key >>3);
	return ret & 0x1F;
}

int main (int argc, char **argv) {

	if(chkparams(argc, argv) == 0){
		printf("Invalid parameters\n");
		exit(0);
	}
	int key = s2i(argv[1]);
	int data= s2i(argv[2]);

	printf("===Geração de subchaves===\n");
	printf(">Input key: %s\n", i2s(key, 10));
	int p10out = p10(key);
	//printf(">P10 in: %s\n", i2s(key, 10));
	printf(">P10 out: %s\n", i2s(p10out, 10));
	int ls1hout = ls1(p10out >> 5);
	int ls1lout = ls1(p10out & 0x1F);
	//printf("LS-1 HIGH in:  %s\n", i2s(p10out >> 5, 5));
	printf(">LS-1 HIGH out: %s\n", i2s(ls1hout, 5));
	//printf("LS-1 LOW in:   %s\n", i2s(p10out & 0x1F, 5));
	printf(">LS-1 LOW out:  %s\n", i2s(ls1lout, 5));
	int ls2hout = ls2(ls1hout);
	int ls2lout = ls2(ls1lout);
	printf(">LS-2 HIGH out: %s\n", i2s(ls2hout, 5));
	printf(">LS-2 LOW out:  %s\n", i2s(ls2lout, 5));
	int ls1hl = (ls1hout << 5) | (ls1lout);
	int ls2hl = (ls2hout << 5) | (ls2lout);
	printf(">ls1hl = %s\n", i2s(ls1hl, 8));
	printf(">ls2hl = %s\n", i2s(ls2hl, 8));
	int key1 = p8(ls1hl);
	int key2 = p8(ls2hl);
	printf(">K1 = %s\n", i2s(key1, 8));
	printf(">K2 = %s\n", i2s(key2, 8));

	printf("=== Cifragem ===\n");
	printf(">Input data: %s\n", i2s(data, 8));
	int ipout = ip(data);
	printf(">Initial Permutation: %s\n", i2s(ipout, 8));
	int epout = ep(ipout&0xF);
	printf(">E/P out: %s\n", i2s(epout, 8));
	int k1xor = (key1 ^ epout) & 0xFF;
	printf(">Key XOR out: %s\n", i2s(k1xor, 8));
	int s0out = box((k1xor>>4) & 0xF, s0);
	int s1out = box((k1xor & 0xF), s1);
	printf(">S0 out: %s\n", i2s(s0out, 2));
	printf(">S1 out: %s\n", i2s(s1out, 2));
	int p4out = p4( (s0out << 2) | s1out );
	printf(">P4 out: %s\n", i2s(p4out, 4));
	int p4xorout = ((ipout >> 4) ^ p4out) & 0xF;
	printf(">P4 xor out: %s\n", i2s(p4xorout, 4));
	int fk1out = (p4xorout << 4) | (ipout&0xF);
	printf(">Fk1 out: %s\n", i2s(fk1out, 8));

	int swout = sw(fk1out);
	printf(">SW out: %s\n", i2s(swout, 8));
	
	int epout2 = ep(swout&0xF);
	printf(">E/P out: %s\n", i2s(epout2, 8));
	int k2xor = (key2 ^ epout2) & 0xFF;
	printf(">Key XOR out: %s\n", i2s(k2xor, 8));
	int s0out2 = box((k2xor>>4) & 0xF, s0);
	int s1out2 = box((k2xor & 0xF), s1);
	printf(">S0 out: %s\n", i2s(s0out2, 2));
	printf(">S1 out: %s\n", i2s(s1out2, 2));
	int p4out2 = p4( (s0out2 << 2) | s1out2 );
	printf(">P4 out: %s\n", i2s(p4out2, 4));
	int p4xorout2 = ((swout >> 4) ^ p4out2) & 0xF;
	printf(">P4 xor out: %s\n", i2s(p4xorout2, 4));
	int fk2out = (p4xorout2 << 4) | (swout&0xF);
	printf(">Fk2 out: %s\n", i2s(fk2out, 8));

	int encrypted = ipi(fk2out);
	printf("Encrypted text: %s\n", i2s(encrypted, 8));
	printf("========================");
	printf("=== Decrypt ===");

	int key1d = key2;
	int key2d = key1;

	printf(">Input data: %s\n", i2s(encrypted, 8));
	ipout = ip(encrypted);
	printf(">Initial Permutation: %s\n", i2s(ipout, 8));
	epout = ep(ipout&0xF);
	printf(">E/P out: %s\n", i2s(epout, 8));
	k1xor = (key1d ^ epout) & 0xFF;
	printf(">Key XOR out: %s\n", i2s(k1xor, 8));
	s0out = box((k1xor>>4) & 0xF, s0);
	s1out = box((k1xor & 0xF), s1);
	printf(">S0 out: %s\n", i2s(s0out, 2));
	printf(">S1 out: %s\n", i2s(s1out, 2));
	p4out = p4( (s0out << 2) | s1out );
	printf(">P4 out: %s\n", i2s(p4out, 4));
	p4xorout = ((ipout >> 4) ^ p4out) & 0xF;
	printf(">P4 xor out: %s\n", i2s(p4xorout, 4));
	fk1out = (p4xorout << 4) | (ipout&0xF);
	printf(">Fk1 out: %s\n", i2s(fk1out, 8));

	swout = sw(fk1out);
	printf(">SW out: %s\n", i2s(swout, 8));
	
	epout2 = ep(swout&0xF);
	printf(">E/P out: %s\n", i2s(epout2, 8));
	k2xor = (key2d ^ epout2) & 0xFF;
	printf(">Key XOR out: %s\n", i2s(k2xor, 8));
	s0out2 = box((k2xor>>4) & 0xF, s0);
	s1out2 = box((k2xor & 0xF), s1);
	printf(">S0 out: %s\n", i2s(s0out2, 2));
	printf(">S1 out: %s\n", i2s(s1out2, 2));
	p4out2 = p4( (s0out2 << 2) | s1out2 );
	printf(">P4 out: %s\n", i2s(p4out2, 4));
	p4xorout2 = ((swout >> 4) ^ p4out2) & 0xF;
	printf(">P4 xor out: %s\n", i2s(p4xorout2, 4));
	fk2out = (p4xorout2 << 4) | (swout&0xF);
	printf(">Fk2 out: %s\n", i2s(fk2out, 8));

	int decrypted = ipi(fk2out);
	printf("Decrypted text: %s\n", i2s(decrypted, 8));
	printf("========================\n");
	if(data == decrypted){
		printf("OK!!\n");
	}else{
		printf("Failed..\n");
	}
}
